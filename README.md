Clone in .vim
copy .vimrc to ~

Install:
sudo apt-get install build-essential cmake
sudo apt-get install python-dev
sudo apt-get install silversearcher-ag

cd ~/.vim/bundle/YouCompleteMe
./install.py